import { NuxtConfig } from '@nuxt/types';

/* eslint-disable camelcase */
const config: NuxtConfig = () => {
	return {
		target:   'static',
		generate: {
			dir:      'public',
			fallback: '404.html'
		},
		router: {
			// middleware:   [ 'trailing-slash-redirect', 'user-agent' ]
		},
		plugins:      [
			{
				src:  '~/plugins/server.ts'
			}, {
				src:  '~/plugins/client.ts',
				mode: 'client'
			}
		],
		css:            [ 'assets/scss/style.scss' ],
		components:     true,
		buildModules:   [
			'@nuxt/typescript-build',
			'@nuxtjs/pwa',
			'@nuxtjs/svg',
			'@nuxtjs/stylelint-module',
			'@nuxtjs/style-resources'
		],
		modules:        [
			'@nuxtjs/i18n',
			'@nuxtjs/robots',
			'@nuxtjs/sitemap',
			'@nuxtjs/svg-sprite'

			// '~/modules/gtm/index'
		],
		i18n: {
			// vueI18nLoader: true,
			locales:       [
				{
					code: 'ru',
					file: 'ru.ts'
				}, {
					code: 'ua',
					file: 'ua.ts'
				}
			],
			lazy:          true,
			defaultLocale: 'ua',
			langDir:       'locales/'
		},
		svgSprite: {
			input:        '~/assets/svg/',
			elementClass: 'ep-icon'
		},
		styleResources: {
			scss: [ 'assets/scss/util/settings.scss' ]
		},
		build: {
			// extractCSS: true,

			/* analyze: {
				analyzerMode: 'static'
			}, */
			postcss:    {
				syntax:  'postcss-scss',
				plugins: [
					require('autoprefixer'), require('cssnano')({
						preset: [
							'default', {
								discardComments: {
									removeAll: true
								}
							}
						]
					})
				]
			},
			loaders: {
				scss: {
					implementation: require('sass')
				},
				cssModules: {
					modules: {
						localIdentName: process.env.NODE_ENV === 'development' ? '[local]-[hash:base64:12]' : '_[hash:base64:4]'
					}
				}
			}
		},
		sitemap: {
			hostname: 'https://baby-photo.fun',
			gzip:     true,
			i18n:     true
		},
		robots: [
			{
				UserAgent: '*'
			}
		],
		pwa: {
			icon: {
				source:   '[srcDir]/[staticDir]/icon.png',
				fileName: 'icon.png',
				purpose:  'any'
			},
			meta: {
				charset:     'utf-8',
				viewport:    'width=device-width, initial-scale=1',
				mobileApp:   true,
				favicon:     true,
				name:        'Hryhorchuk Kate',
				author:      'Hryhorchuk Kate',
				theme_color: '#00498a',
				lang:        'uk-UA',
				ogSiteName:  'baby-photo.fun',
				ogType:      'website',
				ogTitle:     false
			},
			manifest: {
				name:                    'Hryhorchuk Kate',
				background_color:        '#ffffff',
				lang:                    'uk',
				useWebmanifestExtension: false
			}
		},
		cli: {
			badgeMessages: [ 'Hryhorchuk Kate' ]
		},
		loading: '~/components/loading.vue'
	};
};

export default config;
