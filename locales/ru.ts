import head from './head';
import { KLang } from '@/types/index';
import certificates from '@/data/ru/certificates.json';
import faq from '@/data/ru/faq.json';
import portfolio from '@/data/ru/portfolio.json';
import services from '@/data/ru/services.json';

export default (): KLang => {
	return {
		head: {
			...head({
				title:        'ᐉ  Детский фотограф Днепр • Hryhorchuk Kate • Детское фото • NEWBORN',
				description:  'Нужен детский фотограф? Детская фотосессия, как подготовиться, съемка новорожденного. NEWBORN. Фотосессия по случаю празднования 1 года',
				htmlLang:     'ru',
				lang:         'ru-UA',
				alternate:    'uk-UA',
				url:          'https://baby-photo.fun/ru/',
				alternateUrl: 'https://baby-photo.fun/'
			})
		},
		certificates,
		faq,
		portfolio,
		services
	};
};
