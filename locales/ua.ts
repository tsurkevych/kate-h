import head from './head';
import { KLang } from '@/types/index';
import certificates from '@/data/ua/certificates.json';
import faq from '@/data/ua/faq.json';
import portfolio from '@/data/ua/portfolio.json';
import services from '@/data/ua/services.json';

export default (): KLang => {
	return {
		head: {
			...head({
				title:        'ᐉ  Дитячий фотограф Дніпро • Hryhorchuk Kate • Дитяче фото • NEWBORN',
				description:  'Потрібен дитячий фотограф? Дитяча фотосесія, як підготуватися, зйомка новонародженого. NEWBORN. Фотосесія з нагоди святкування 1 рочку',
				htmlLang:     'uk',
				lang:         'uk-UA',
				alternate:    'ru-UA',
				url:          'https://baby-photo.fun/',
				alternateUrl: 'https://baby-photo.fun/ru/'
			})
		},
		certificates,
		faq,
		portfolio,
		services
	};
};
