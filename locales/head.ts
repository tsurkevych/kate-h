import { MetaInfo } from 'vue-meta';
import { KHead } from '@/types';

const ga = process.env.NODE_ENV === 'production'
	? [
			{
				src:   'https://www.googletagmanager.com/gtag/js?id=G-C5VSTQQGLX',
				async: true
			}, {
				innerHTML: `window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
	  
		gtag('config', 'G-C5VSTQQGLX');`
			}
		]
	: [];

export default (data: KHead): MetaInfo => {
	const {
		title,
		description,
		lang,
		url,
		htmlLang,
		alternate,
		alternateUrl
	}: KHead = data;

	return {
		__dangerouslyDisableSanitizers: [ 'script', 'noscript' ],
		htmlAttrs:                      {
			lang: htmlLang
		},
		title,
		link: [
			{
				rel:      'alternate',
				hreflang: alternate,
				href:     alternateUrl
			},
			{
				rel:  'image_src',
				href: 'https://baby-photo.fun/logo.png'
			},

			{
				rel:  'icon',
				type: 'image/x-icon',
				href: '/favicon.ico'
			}
		],
		meta: [
			{
				name:    'google-site-verification',
				content: 'ZRlaSHG8uHmGH346-RAYCctZEzw9eYweY7SPWo9VA64'
			},
			{
				name:    'keywords',
				content: title
			},
			{
				httpEquiv: 'X-UA-Compatible',
				content:      'ie=edge'
			},
			{
				property: 'og:url',
				content:  'https://baby-photo.fun/'
			},
			{
				property: 'og:locale',
				content:  'uk-UA'
			},
			{
				name:    'robots',
				content: 'index, follow'
			},
			{
				name:    'color',
				content: '#00498a'
			},
			{
				name:    'description',
				content: description
			},
			{
				property: 'og:title',
				content:   title
			},
			{
				property: 'og:description',
				content:  description
			},
			{
				property: 'og:type',
				content:   'website'
			},
			{
				property: 'og:image',
				content:  'https://baby-photo.fun/logo.png'
			},
			{
				property: 'og:url',
				content:  url
			},
			{
				property: 'og:site_name',
				content:  'ualg.org'
			},
			{
				property: 'og:locale',
				content:  lang
			},
			{
				property: 'og:locale:alternate',
				content:  alternate
			}
		],
		script: [
			...ga, {
				innerHTML: JSON.stringify({
					'@context': 'https://schema.org',
					'@graph':   [
						{
							'@type': 'WebSite',
							name:    title,
							url:     'https://baby-photo.fun'
						}, {
							'@type':   'Organization',
							name:      'Hryhorchuk Kate',
							logo:      'https://baby-photo.fun/logo.png',
							description,
							image:     'https://baby-photo.fun/logo.png',
							url:       'https://baby-photo.fun',
							telephone: '+380682029597',
							sameAs:    [ 'https://www.instagram.com/baby_foto.newborn/' ]
						}
					]
				}),
				type: 'application/ld+json'
			}
		]
	};
};
