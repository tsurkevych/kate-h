import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { MetaInfo } from 'vue-meta';

export interface KLang {
	head: MetaInfo;
	[label: string]: any;
}

export interface KHead {
	title: string;
	description: string;
	htmlLang: string;
	lang: string;
	url: string;
	alternate: string;
	alternateUrl: string;
}

export interface KModuleGTM {
	enabled?: boolean | undefined;
	id: string | undefined;
	layer?: string;
	autoInit: boolean;
	variables?: {};
	scriptId?: string;
	scriptDefer?: boolean;
	dev?: boolean;
	scriptURL?: string;
	noScript?: boolean;
	noScriptId?: string;
	noScriptURL?: string;
}

export interface KRootStates {
	ww: number; // ширина вікна
	wh: number; // висота вікна
	scrollTop: number; // позиція від верху сторінки
	loading: boolean; // перехід між сторінками
	margin: string; // відступ з правої сторони при блокуванні скролу
}
export type KRootGetters = GetterTree<KRootStates, KRootStates>;
export type KRootMutations = MutationTree<KRootStates>;
export type KRootActions = ActionTree<KRootStates, KRootStates>;
